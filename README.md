# Config

WIP config for TF2 using https://github.com/ArgosOfIthica/scalu.
## Requirements to build
Python 3.6 installed.
## Compiling

Clone scalu to a folder using the link above. Delete logo.scalu from the [scalu source]/scalu_in folder. Copy all files from the  src/ folder to the same [scalu source]/scalu_in folder. Run ```python scalu.py compile``` for windows, and  ```python3 scalu.py compile``` for linux/osx in the [scalu source]/python folder to compile. Move all config files from [scalu source]/scalu_out to your TF2 config directory.